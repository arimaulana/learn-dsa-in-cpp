/**
 * How to analyze time complexity?
 * 
 * Running time depends upon:
 * Input only (no matter single or multiple processor, read/write speed, 32 or 64 bit)
 * 
 * 1 unit of time for arithmetical and logical operations
 * 1 unit of time for assignment and return
 */

int summarize(int a, int b) {
    /** 
     * The plus operation take 1 unit of time
     * The return statement take 1 unit of time
     * 
     * so the sum is Tsum = 2 (constant time)
     */
    return a + b; // Tsum = 2 (constant time)
}

int sumOfArray(int arr[], int n) {
    /**
     * there would be cost and number of times
     * total = 0, assignment only, so it takes 1 unit of time, and only take 1 times. in math => 1
     * for loop, there logical (<=) and arithmetic (++) operator, so it takes 2 unit of time, and it will take n times (i until n). in math => 2n
     * inside loop, assignment and plus operation, so it takes 2 unit of time, and will take n times. in math => 2n
     */
    int total = 0;  // cost 1, number of times 1
    for (int i = 1; i <= n; i++) { // 2 (comparison <= and ++ operator), n number of times
        total = total + arr[i];
    }
    return total;
}