#include <stdio.h>

int main() {
    int i, x, result, left, right, mid, n, nr_queries;
    int arr[100005];

    scanf("%d%d", &n, &nr_queries);

    for (i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    // let's take each query
    for (i = 1; i <= nr_queries; i++) {
        scanf("%d", &x); // read the number we are searching

        result = -1;
        left = 0;
        right = n - 1;

        while (left <= right) {
            mid = (left + right) / 2;
            
            if (arr[mid] == x) {
                result = mid;
                right = mid - 1;
            } else if (arr[mid] < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        printf("%d\n", result);
    }

    return 0;
}