// 03_variable_scope.cpp change this into main.cpp if u want debug/build this code
#include <bits/stdc++.h>

using namespace std;

int arr[3][3];

// adding value x to every element in the matrix arr
void addMatrix(int x) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            arr[i][j] += x;
        }
    }
}

// return the sum of all elements of arr
int sumMatrix() {
    int sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            sum += arr[i][j];
        }
    }
    return sum;
}

int main() {
    int sum = 0;
    addMatrix(1);
    sum = sumMatrix();

    addMatrix(10);
    sum = sumMatrix();

    addMatrix(100);
    sum = sumMatrix();

    cout << sum;

    return 0;
}