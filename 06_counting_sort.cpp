#include <bits/stdc++.h>

using namespace std;

ifstream f("06_data.in");
ofstream g("06_data.out");
int arr[101], x, n; // i supposed that we have numbers from 0-100
int maximum;

void main() {
    f >> n; // number of elements
    for (int i = 1; i <= n; i++) {
        f >> x; // a new number
        arr[x]; // increasing the appearance array

        maximum = max(maximum, x);
    }

    for (int i = 0; i <= maximum; i++) {
        if (arr[i] > 0) {
            for (int j = 1; j <= arr[i]; j++) {
                cout << i << " ";
            }
        }
    }
}