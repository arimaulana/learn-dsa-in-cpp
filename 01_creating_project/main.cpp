#include <iostream>
#include <bits/stdc++.h> // to read and write into from a file

using namespace std;

// f and g are the tags of the file
ifstream f("data.in");
ofstream g("data.out");

int main() {
    int a, b, sum;

    f >> a >> b;
    sum = a + b;

    g << sum;

    return 0;
}