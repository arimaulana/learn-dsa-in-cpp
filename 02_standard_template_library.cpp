#include <bits/stdc++.h>
using namespace std;

int main() {
    int a = 5, b = 8, maximum;

    // max from a and b
    maximum = max(a, b);

    // swap 2 values
    swap(a, b);

    // double pow (double base, double exponent)
    int number = 2;
    double cubic_root;
    
    cubic_root = pow((double) (number), 1.0/3);

    cout << cubic_root << "\n";
    cout << fixed << setprecision(10) << cubic_root << "\n";
    cout << fixed << setprecision(2) << cubic_root << "\n";

    return 0;
}